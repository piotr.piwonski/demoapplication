package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        System.out.println("Start application Piotr");
        System.out.println("Line from branch develop_2");
        System.out.println("Second line added");
        System.out.println(new Date());
        SpringApplication.run(DemoApplication.class, args);
    }

}
